import * as React from "react";
import trelloImage from "../assets/Trello_white_logo.svg";
import useStyles from "../styles/style";
import { NavLink } from "react-router-dom";
import { Toolbar, Typography, Box, AppBar } from "@mui/material";

const Header = ({ isBoards }) => {
  const classes = useStyles();

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="fixed" sx={{ display: "flex" }}>
        <Toolbar>
          <Typography variant="h6" color="inherit" component="div" sx={{ flexGrow: "1" }}>
            {!isBoards ? (
              `Welcome to Trello`
            ) : (
              <NavLink to="/" className={classes.boardsLink}>
                Boards
              </NavLink>
            )}
          </Typography>

          <div className={classes.imageDiv}>
            <img className={classes.image} src={trelloImage} alt="trello logo" />
          </div>
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default Header;
