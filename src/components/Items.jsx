import React, { useEffect, useState, useContext } from "react";
import { Box, Button, TextField } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { addItem, deleteItem, getItems } from "../api";
import Item from "./Item";
import ErrorContext from "../contexts/ErrorContext";

const Items = ({ checkListId, cardId, items, setItems }) => {
  const { setError, setIsLoading } = useContext(ErrorContext);
  const [isInputVisible, setInputVisible] = useState(false);
  const [newItem, setNewItem] = useState("");

  useEffect(() => {
    //function that fetches all items
    async function fetchItems() {
      try {
        setIsLoading(true);
        let res = await getItems(checkListId);
        setItems(res.data);
        setIsLoading(false);
      } catch (error) {
        setIsLoading(false);
        setError({ message: "while fetching items" });
        console.log("error while fetching items:", error);
      }
    }
    fetchItems();
  }, []);

  const toggleInputVisibility = () => {
    setInputVisible(!isInputVisible);
  };

  //function that creates a item
  const addItemHandler = async () => {
    try {
      setIsLoading(true);
      let newItemAdded = await addItem(checkListId, newItem);
      setItems([...items, newItemAdded.data]);
      setNewItem("");
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      setError({ message: "while adding item" });
      console.log("error while adding item:", error);
    }
  };

  //function that deletes the item
  const deleteItemHandler = async (itemId, isChecked) => {
    try {
      setIsLoading(true);
      await deleteItem(checkListId, itemId);
      setItems(items.filter((item) => item.id !== itemId));
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      setError({ message: "while deleting item" });
      console.log("error while deleting item:", error);
    }
  };

  return (
    <>
      {/* All Items */}
      {items.map((item) => {
        return <Item items={items} setItems={setItems} cardId={cardId} state={item.state} checkListId={checkListId} item={item} key={item.id} deleteItemHandler={deleteItemHandler}></Item>;
      })}

      <Box maxWidth={200}>
        {!isInputVisible && (
          <Button
            startIcon={<AddIcon />}
            onClick={toggleInputVisibility}
            sx={{ marginBottom: 1, color: "black", backgroundColor: "lightgrey", textTransform: "none", marginLeft: 1 }}
            variant="contained"
            size="small"
          >
            Add Item
          </Button>
        )}
        {isInputVisible && (
          <Box sx={{ display: "flex", flexDirection: "column" }}>
            <form
              onSubmit={(e) => {
                e.preventDefault();
                addItemHandler();
              }}
            >
              <TextField label="" variant="outlined" size="small" value={newItem} onChange={(e) => setNewItem(e.target.value)} autoFocus autoComplete="off" required />
              <Box sx={{ display: "flex", alignItems: "center", marginTop: 1 }}>
                <Button type="submit" variant="contained" color="primary" sx={{ textTransform: "none" }}>
                  Add
                </Button>
                <Button onClick={toggleInputVisibility} variant="text" sx={{ backgroundColor: "none", color: "black", margin: 1, textTransform: "none" }}>
                  Cancel
                </Button>
              </Box>
            </form>
          </Box>
        )}
      </Box>
    </>
  );
};

export default Items;
