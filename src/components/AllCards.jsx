import React, { useEffect, useState, useContext } from "react";
import { Button, Card, Typography, TextField, Box, IconButton, Dialog, DialogContent, Grid, Snackbar, Tooltip } from "@mui/material";
import { grey } from "@mui/material/colors";
import useStyles from "../styles/style";
import { addCard, addChecklist, deleteCard, getAllCards } from "../api";
import AllCheckLists from "./AllCheckLists";
import SnackContext from "../contexts/SnackContext";
import CreditCardIcon from "@mui/icons-material/CreditCard";
import AddIcon from "@mui/icons-material/Add";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
import CloseIcon from "@mui/icons-material/Close";
import ErrorContext from "../contexts/ErrorContext";
import ArchiveIcon from "@mui/icons-material/Archive";
import InputForm from "../helpers/InputForm";

const AllCards = ({ listId }) => {
  const { setError, setIsLoading } = useContext(ErrorContext);
  const { open: opened, message, showSnackbar, closeSnackbar } = useContext(SnackContext);
  const classes = useStyles();
  const [cards, setCards] = useState([]);
  const [newCardName, setNewCardName] = useState("");
  const [isInputVisible, setInputVisible] = useState(false);
  const [selectedCard, setSelectedCard] = useState("");
  const [open, setOpen] = React.useState(false);
  const [checkLists, setCheckLists] = useState([]);
  const [openChecklistDialog, setOpenChecklistDialog] = useState(false);
  const [newChecklistName, setNewChecklistName] = useState("");

  const handleClickOpen = (card) => {
    setOpen(true);
    setSelectedCard(card);
  };
  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    // function that fetches all cards
    async function getCards() {
      try {
        setIsLoading(true);
        let cards = await getAllCards(listId);
        setCards(cards.data);
        setIsLoading(false);
      } catch (error) {
        setIsLoading(false);
        setError({ message: "while fetching cards" });
        console.log(`error while fetching all cards in ${listId}`, error);
      }
    }
    getCards();
  }, []);

  const toggleInputVisibility = () => {
    setInputVisible(!isInputVisible);
  };

  const handleChecklistDialogOpen = () => {
    setOpenChecklistDialog(true);
  };

  const handleChecklistDialogClose = () => {
    setOpenChecklistDialog(false);
  };

  //function that creates a new card
  const addCardHandler = async (listId) => {
    try {
      setIsLoading(true);
      let newCardAdded = await addCard(listId, newCardName);
      setNewCardName("");
      setCards([...cards, newCardAdded.data]);
      toggleInputVisibility();
      showSnackbar("successfully added the card");
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      setError({ message: "while creating new card" });
      console.log("error while adding card", error);
    }
  };

  //function that creates a new checklist
  const addChecklistHandler = async (cardId) => {
    try {
      setIsLoading(true);
      let newCheckListAdded = await addChecklist(cardId, newChecklistName);
      setCheckLists([...checkLists, newCheckListAdded.data]);
      setNewChecklistName("");
      handleChecklistDialogClose();
      showSnackbar("sucessfully added the checklist");
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      setError({ message: "while creating new checklist" });
      console.log("error while adding checklist", error);
    }
  };

  //function that deletes the card
  const deleteCardHandler = async (cardId) => {
    try {
      setIsLoading(true);
      await deleteCard(cardId);
      setCards(cards.filter((card) => card.id !== cardId));
      handleClose();
      showSnackbar("successfully deleted the card");
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      setError({ message: "while deleting a card" });
      console.log("error while deleting card:", error);
    }
  };

  const clickHandler = (e, id, card) => {
    if (e.target.id === "delete-card" || e.target.closest("#delete-card")) {
      deleteCardHandler(id);
    } else {
      handleClickOpen(card);
    }
  };

  return (
    <>
      {cards.map((card) => {
        return (
          <div key={card.id}>
            <Card
              onClick={(e) => clickHandler(e, card.id, card)}
              sx={{ padding: 1, marginBottom: 1, boxShadow: "0 0 2px black", borderRadius: "0.5rem", display: "flex", width: "130%" }}
              key={card.id}
            >
              <Typography flexGrow={1}>{card.name}</Typography>
              <Tooltip title="delete card">
                <ArchiveIcon id="delete-card" sx={{ padding: 0.5 }}></ArchiveIcon>
              </Tooltip>
            </Card>

            {/* Dialog will open when we click on specific card */}
            <div className={classes.dialog}>
              <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                sx={{
                  "& .MuiBackdrop-root": {
                    backgroundColor: "rgba(0, 0, 0, 0.2)",
                  },
                }}
              >
                <div style={{ display: "flex", alignItems: "center", padding: "16px", backgroundColor: grey[200], width: "600px" }}>
                  <div style={{ display: "flex", alignItems: "center", marginRight: "auto" }}>
                    <CreditCardIcon sx={{ marginLeft: 1.5, color: "grey" }} />
                    <Typography variant="h5" style={{ marginLeft: "8px", marginRight: "20rem" }}>
                      {selectedCard.name}
                    </Typography>
                  </div>
                  <IconButton onClick={handleClose}>
                    <Tooltip title="close card">
                      <CloseIcon />
                    </Tooltip>
                  </IconButton>
                </div>
                <DialogContent sx={{ backgroundColor: grey[200] }}>
                  {/* Main content */}
                  <Grid container spacing={2}>
                    <Grid item xs={9}>
                      <Typography variant="body1">Checklists</Typography>
                      <AllCheckLists checkLists={checkLists} setCheckLists={setCheckLists} card={card}></AllCheckLists>
                    </Grid>
                    {/* Side bar */}
                    <Grid item xs={3}>
                      <Box pb={2}>
                        <Typography variant="body2" align="left" gutterBottom>
                          Add to Card
                        </Typography>
                        <Button startIcon={<CheckBoxIcon></CheckBoxIcon>} variant="contained" size="small" color="grey" onClick={handleChecklistDialogOpen} sx={{ textTransform: "none" }}>
                          Checklist
                        </Button>

                        <InputForm
                          open={openChecklistDialog}
                          onClose={handleChecklistDialogClose}
                          submitHandler={(e) => {
                            e.preventDefault();
                            addChecklistHandler(card.id);
                          }}
                          title="Add Checklist"
                          label=""
                          value={newChecklistName}
                          onChange={(e) => setNewChecklistName(e.target.value)}
                        />
                      </Box>

                      <Button variant="outlined" size="small" color="error" onClick={() => deleteCardHandler(selectedCard.id)}>
                        Archive
                      </Button>
                    </Grid>
                  </Grid>
                </DialogContent>
              </Dialog>
            </div>
          </div>
        );
      })}

      <Box maxWidth={200}>
        {!isInputVisible && (
          <Button
            onClick={toggleInputVisibility}
            startIcon={<AddIcon sx={{}} />}
            sx={{
              color: grey[700],
              backgroundColor: grey[100],
              textAlign: "left",
              padding: "0 3rem 0 0.5rem",
              fontSize: "0.8rem",
              textTransform: "none",
            }}
            variant="filled"
          >
            Add a card
          </Button>
        )}
        {isInputVisible && (
          <Box sx={{ display: "flex", flexDirection: "column" }}>
            <form
              onSubmit={(e) => {
                e.preventDefault();
                addCardHandler(listId);
              }}
            >
              <TextField
                label=""
                size="small"
                InputLabelProps={{ sx: { color: "black" } }}
                InputProps={{ sx: { color: "black" } }}
                value={newCardName}
                onChange={(e) => setNewCardName(e.target.value)}
                autoFocus
                style={{ width: "130%" }}
                autoComplete="off"
                required
              />
              <Box sx={{ display: "flex", alignItems: "center", marginTop: 1 }}>
                <Button type="submit" variant="contained" color="primary" sx={{ textTransform: "none", padding: "0.2rem 0.6rem" }}>
                  Add Card
                </Button>
                <IconButton onClick={toggleInputVisibility}>
                  <CloseIcon />
                </IconButton>
              </Box>
            </form>
          </Box>
        )}
      </Box>
      <Snackbar open={opened} autoHideDuration={2000} onClose={closeSnackbar} message={message}></Snackbar>
    </>
  );
};

export default AllCards;
