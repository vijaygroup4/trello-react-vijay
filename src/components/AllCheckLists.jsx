import React, { useContext, useEffect } from "react";
import { deleteChecklist, getChecklists } from "../api";
import { Typography, Snackbar } from "@mui/material";
import SnackContext from "../contexts/SnackContext";
import ErrorContext from "../contexts/ErrorContext";
import CheckList from "./CheckList";

const AllCheckLists = ({ card, checkLists, setCheckLists }) => {
  const { setError, setIsLoading } = useContext(ErrorContext);
  const { open, message, showSnackbar, closeSnackbar } = useContext(SnackContext);

  useEffect(() => {
    //function that fetches all checklists
    async function getCheckLists() {
      try {
        setIsLoading(true);
        let res = await getChecklists(card.id);
        setCheckLists(res.data);
        setIsLoading(false);
      } catch (error) {
        setIsLoading(false);
        setError({ message: "while fetching checklists" });
        console.log("error while fetching checklists", error);
      }
    }
    getCheckLists();
  }, []);

  //function that delete a checklist
  const deleteCheckListHandler = async (checkListId) => {
    try {
      setIsLoading(true);
      await deleteChecklist(checkListId);
      setCheckLists(checkLists.filter((checklist) => checklist.id !== checkListId));
      showSnackbar("successfully deleted checklist");
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      setError({ message: "while deleting checklist" });
      console.log("error while deleting checklist:", error);
    }
  };

  return (
    <>
      {/* Checklists */}
      {checkLists.map((checklist) => {
        return <CheckList cardId={card.id} checklist={checklist} deleteCheckListHandler={deleteCheckListHandler} key={checklist.id}></CheckList>;
      })}

      {checkLists.length === 0 && <Typography>No Checklists found...</Typography>}
      <Snackbar open={open} autoHideDuration={2000} onClose={closeSnackbar} message={message}></Snackbar>
    </>
  );
};

export default AllCheckLists;
