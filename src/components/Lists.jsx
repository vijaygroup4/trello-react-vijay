import React, { useContext, useState } from "react";
import { Card, CardHeader, Typography, IconButton, Menu, MenuItem, CardContent, Snackbar } from "@mui/material";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import AllCards from "../components/AllCards";
import SnackContext from "../contexts/SnackContext";
import { deleteList } from "../api";
import { grey } from "@mui/material/colors";
import ErrorContext from "../contexts/ErrorContext";

const options = ["Delete list"];

const Lists = ({ listsDetails, setListDetails }) => {
  const { setError, setIsLoading } = useContext(ErrorContext);
  const { open, message, showSnackbar, closeSnackbar } = useContext(SnackContext);
  const [anchorEl, setAnchorEl] = useState(null);
  const [selectedListId, setSelectedListId] = useState(null);

  const handleClick = (event, listId) => {
    setAnchorEl(event.currentTarget);
    setSelectedListId(listId);
  };

  const handleClose = () => {
    setAnchorEl(null);
    setSelectedListId(null);
  };

  //function that deletes a list
  const deleteListHandler = async (listId) => {
    try {
      setIsLoading(true);
      await deleteList(listId);
      setListDetails(listsDetails.filter((listDetail) => listDetail.id !== listId));
      showSnackbar("successfully deleted the list");
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      setError({ message: "while deleting the list" });
      console.log(`error while deleting list:${listId}`, error);
    }
  };

  return (
    <>
      {/* All Lists */}
      {listsDetails.map((list) => (
        <Card key={list.id} sx={{ minWidth: 250, margin: "0", padding: "0", cursor: "pointer", backgroundColor: grey[100], height: "fit-content", borderRadius: "20px" }}>
          <CardHeader
            action={
              <IconButton onClick={(event) => handleClick(event, list.id)}>
                <MoreHorizIcon></MoreHorizIcon>
              </IconButton>
            }
            subheader={<Typography variant="body2">{list.name}</Typography>}
            color="primary"
          />
          <CardContent sx={{ display: "flex", flexDirection: "column", justifyContent: "flex-start", width: 200 }}>
            <AllCards listId={list.id} />
          </CardContent>
        </Card>
      ))}
      {/* Menu that appears when we click on 3 dots */}
      <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={handleClose}>
        {options.map((option) => (
          <MenuItem
            key={option}
            onClick={() => {
              deleteListHandler(selectedListId);
              handleClose();
            }}
            sx={{ padding: "0.1rem 0.8rem" }}
          >
            {option}
          </MenuItem>
        ))}
      </Menu>
      <Snackbar open={open} autoHideDuration={2000} onClose={closeSnackbar} message={message}></Snackbar>
    </>
  );
};

export default Lists;
