import React, { useContext, useState } from "react";
import AddCircleRoundedIcon from "@mui/icons-material/AddCircleRounded";
import { Box, TextField, Button, Dialog, Typography, Snackbar } from "@mui/material";
import SnackContext from "../../contexts/SnackContext";
import ErrorContext from "../../contexts/ErrorContext";
import { addBoard } from "../../api";
import InputForm from "../../helpers/InputForm";

const style = {
  width: 300,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 1,
  "& > :not(style)": { m: 1, width: "25ch" },
};

const CreateBoard = ({ boards, setBoards }) => {
  const { setError, setIsLoading } = useContext(ErrorContext);
  const { open: opened, message, showSnackbar, closeSnackbar } = useContext(SnackContext);
  const [newBoard, setNewBoard] = useState("");
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  //function that creates the board
  const submitHandler = (e) => {
    e.preventDefault();
    async function createBoard() {
      try {
        setIsLoading(true);
        let newBoardAdded = await addBoard(newBoard);
        setBoards([...boards, newBoardAdded.data]);
        showSnackbar("successfully created the board");
        setNewBoard("");
        setIsLoading(false);
      } catch (error) {
        setIsLoading(false);
        setError({ message: "while creating a board" });
        console.log("error while adding board:", error);
      }
    }
    handleClose();
    createBoard();
  };

  return (
    <>
      <Box sx={{ "& > :not(style)": { m: 1 }, display: "flex" }}>
        <Button
          startIcon={<AddCircleRoundedIcon></AddCircleRoundedIcon>}
          onClick={handleOpen}
          variant="outlined"
          sx={{ width: 220, height: 50, fontSize: "0.8rem", display: "flex", justifyContent: "flex-start", alignItems: "center", alignSelf: "center", textTransform: "none" }}
        >
          Add a Board
        </Button>
      </Box>

      <InputForm open={open} onClose={handleClose} submitHandler={submitHandler} title="Enter Board name:" label="" value={newBoard} onChange={(e) => setNewBoard(e.target.value)} />
      <Snackbar open={opened} autoHideDuration={2000} onClose={closeSnackbar} message={message}></Snackbar>
    </>
  );
};

export default CreateBoard;
