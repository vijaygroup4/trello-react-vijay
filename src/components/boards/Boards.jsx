import React, { useContext } from "react";
import CreateBoard from "./CreateBoard";
import DeleteIcon from "@mui/icons-material/Delete";
import { useNavigate } from "react-router-dom";
import { Box, Tooltip, Typography, CardMedia, CardContent, Card, styled, Stack, Paper, Snackbar } from "@mui/material";
import SnackContext from "../../contexts/SnackContext";
import { deleteBoard } from "../../api";
import ErrorContext from "../../contexts/ErrorContext";

export const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "start",
  color: theme.palette.text.secondary,
}));

const Boards = ({ boards, setBoards }) => {
  const navigate = useNavigate();
  const { error, setError, isLoading, setIsLoading } = useContext(ErrorContext);
  const { open, message, showSnackbar, closeSnackbar } = useContext(SnackContext);

  // function to delete the board
  const deleteBoardHandler = async (id) => {
    try {
      setIsLoading(true);
      await deleteBoard(id);
      setBoards(boards.filter((board) => board.id !== id));
      showSnackbar("successfully deleted the board");
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      setError({ message: "while deleting a board" });
      console.log("error while deleting:", error);
    }
  };

  //function that triggers the deleteHandler function or navigates to specific board
  const clickHandler = (e, id, boardName) => {
    if (e.target.id === "delete-board" || e.target.closest("#delete-board")) {
      deleteBoardHandler(id);
    } else {
      navigate(`/boards/${id}`, {
        state: {
          boardName,
        },
      });
    }
  };

  return (
    <Box display="flex" gap={2} paddingLeft={1} paddingTop={2} flexWrap="nowrap">
      {boards.map((board) => {
        return (
          <Card key={board.id} onClick={(e) => clickHandler(e, board.id, board.name)} sx={{ minWidth: 200, margin: "0", padding: "0", cursor: "pointer" }}>
            <CardMedia sx={{ height: 140, width: 200 }} image={`https://source.unsplash.com/random/${board.id}`} title="random images">
              <CardContent sx={{ display: "flex" }}>
                <Tooltip title="Jump into lists...">
                  <Typography gutterBottom variant="h5" component="div" color="white" sx={{ flexGrow: "1", textShadow: "1px 1px 2px rgba(0, 0, 0, 0.8)", fontSize: "1.2rem" }}>
                    {board.name}
                  </Typography>
                </Tooltip>

                <Tooltip title="Delete">
                  <DeleteIcon sx={{ color: "red" }} id="delete-board"></DeleteIcon>
                </Tooltip>
              </CardContent>
            </CardMedia>
          </Card>
        );
      })}
      <CreateBoard boards={boards} setBoards={setBoards} isLoading={isLoading} setIsLoading={setIsLoading}></CreateBoard>

      {!isLoading && boards.length === 0 && !error && (
        <Box sx={{ display: "flex", justifyContent: "center", alignItems: "center", paddingLeft: 40, paddingTop: 10 }}>
          <Typography color="black" sx={{ textAlign: "center" }}>
            Oops! No Boards Found. Please add...
          </Typography>
        </Box>
      )}

      <Snackbar open={open} autoHideDuration={2000} onClose={closeSnackbar} message={message}></Snackbar>
    </Box>
  );
};

export default Boards;
