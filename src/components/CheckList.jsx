import React, { useState } from "react";
import { Box, Button, IconButton, Stack, Typography, LinearProgress } from "@mui/material";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
import Items from "./Items";

const CheckList = ({ checklist, deleteCheckListHandler, cardId }) => {
  const [items, setItems] = useState([]);

  let percValue = Math.floor(items.length > 0 ? (items.filter((item) => item.state === "complete").length / items.length) * 100 : 0);

  return (
    <Stack key={checklist.id} display="flex" flexDirection="column" spacing={1}>
      <Box display="flex" alignItems="center" sx={{ width: "100%" }}>
        <IconButton size="small">
          <CheckBoxIcon sx={{ fontSize: "1.5rem" }}></CheckBoxIcon>
        </IconButton>
        <Typography>{checklist.name}</Typography>
        <Button
          variant="contained"
          sx={{
            color: "black",
            backgroundColor: "lightgray",
            marginRight: "0",
            marginLeft: "auto",
            textTransform: "none",
          }}
          size="small"
          onClick={() => deleteCheckListHandler(checklist.id)}
        >
          Delete
        </Button>
      </Box>
      <Box sx={{ width: "100%" }}>
        <Typography variant="subtitle2" fontSize={10}>
          {percValue}%
        </Typography>
        <LinearProgress variant="determinate" value={percValue} />
      </Box>{" "}
      {/* All Items */}
      <Items items={items} setItems={setItems} cardId={cardId} checkListId={checklist.id}></Items>
    </Stack>
  );
};

export default CheckList;
