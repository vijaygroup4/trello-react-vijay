import React, { useState, useContext } from "react";
import { Typography, Checkbox, Box, Tooltip } from "@mui/material";
import DeleteOutlineOutlinedIcon from "@mui/icons-material/DeleteOutlineOutlined";
import { completeCheck, incompleteCheck } from "../api";
import ErrorContext from "../contexts/ErrorContext";

const Item = ({ item, deleteItemHandler, cardId, checkListId, state, items, setItems }) => {
  const [isChecked, setIsChecked] = useState(state === "complete" ? true : false);
  const { setError, setIsLoading } = useContext(ErrorContext);

  //function that toggles the checked state for each checkbox
  const handleCheckboxChange = async () => {
    try {
      if (!isChecked) {
        setIsLoading(true);
        await completeCheck(cardId, checkListId, item.id);
        let newItems = items.filter((innerItem) => {
          if (innerItem.id === item.id) {
            innerItem.state = "complete";
          }
          return innerItem;
        });
        setItems[newItems];
        setIsLoading(false);
      } else {
        setIsLoading(true);
        await incompleteCheck(cardId, checkListId, item.id);
        let newItems = items.filter((innerItem) => {
          if (innerItem.id === item.id) {
            innerItem.state = "incomplete";
          }
          return innerItem;
        });
        setItems[newItems];
        setIsLoading(false);
      }
    } catch (error) {
      setIsLoading(false);
      setError({ message: "while changing the state of a checkitem" });
      console.log("error while setting checked state of item:", error);
    }
    setIsChecked(!isChecked);
  };

  return (
    <Box key={item.id} display="flex" alignItems="center">
      <Checkbox checked={isChecked} onChange={handleCheckboxChange} sx={{ padding: 0.5 }} />
      <Typography sx={{ textDecoration: isChecked ? "line-through" : "none", overflowX: "hidden" }}>{item.name}</Typography>
      <Tooltip title="delete item">
        <DeleteOutlineOutlinedIcon sx={{ padding: 0.5, marginRight: 0, marginLeft: "auto", cursor: "pointer" }} onClick={() => deleteItemHandler(item.id, isChecked)}></DeleteOutlineOutlinedIcon>
      </Tooltip>
    </Box>
  );
};

export default Item;
