import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";
import { BrowserRouter } from "react-router-dom";
import { CssBaseline } from "@mui/material";
import SnackContextProvider from "./contexts/SnackContextProvider.jsx";
import ErrorContextProvider from "./contexts/ErrorContextProvider.jsx";

ReactDOM.createRoot(document.getElementById("root")).render(
  <BrowserRouter>
    <SnackContextProvider>
      <ErrorContextProvider>
        <CssBaseline />
        <App />
      </ErrorContextProvider>
    </SnackContextProvider>
  </BrowserRouter>
);
