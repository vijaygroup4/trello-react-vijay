import HomePage from "./pages/HomePage";
import "./App.css";
import { Routes, Route } from "react-router-dom";
import Header from "./components/Header";
import BoardDetails from "./pages/BoardDetails";
import PageNotFound from "./pages/PageNotFound";
import useStyles from "./styles/style";

function App() {
  const classes = useStyles();

  return (
    <>
      <Routes>
        <Route path="/" element={<HomePage></HomePage>}></Route>
        <Route
          path="/boards/:id"
          element={
            <>
              <Header isBoards="true"></Header>
              <BoardDetails></BoardDetails>
            </>
          }
        ></Route>
        <Route path="*" element={<PageNotFound></PageNotFound>}></Route>
      </Routes>
    </>
  );
}

export default App;
