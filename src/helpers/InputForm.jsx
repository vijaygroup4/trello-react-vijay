import React, { useState } from "react";
import { TextField, Button, Box, Dialog, DialogActions, DialogContent, DialogTitle } from "@mui/material";

const InputForm = ({ open, onClose, submitHandler, title, label, value, onChange }) => {
  return (
    <Dialog open={open} onClose={onClose}>
      <form onSubmit={submitHandler}>
        <DialogTitle>{title}</DialogTitle>
        <DialogContent>
          <TextField label={label} size="small" value={value} onChange={onChange} autoFocus autoComplete="off" required />
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose} sx={{ textTransform: "none" }}>
            Cancel
          </Button>
          <Button type="submit" color="primary" sx={{ textTransform: "none" }}>
            Add
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

export default InputForm;
