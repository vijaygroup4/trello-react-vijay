import axios from "axios";

export const apiKey = import.meta.env.REACT_APP_API_KEY;
export const token = import.meta.env.REACT_APP_TOKEN;

axios.defaults.params = {
  key: apiKey,
  token: token,
};

const base_URL = `https://api.trello.com/1`;

//getting boards
export const getBoards = async () => {
  const boards_url = `${base_URL}/members/me/boards`;
  try {
    let res = await axios.get(boards_url);
    return res;
  } catch (error) {
    throw error;
  }
};

//getting lists
export const getLists = async (id) => {
  const get_lists_url = `${base_URL}/boards/${id}/lists`;
  try {
    let res = await axios.get(get_lists_url);
    return res;
  } catch (error) {
    throw error;
  }
};

//deleting board
export const deleteBoard = async (id) => {
  const delete_board_url = `${base_URL}/boards/${id}`;
  try {
    await axios.delete(delete_board_url);
  } catch (error) {
    throw error;
  }
};

//adding board
export const addBoard = async (newBoard) => {
  const add_board_url = `${base_URL}/boards/?name=${newBoard}`;
  try {
    let res = await axios.post(add_board_url);
    return res;
  } catch (error) {
    throw error;
  }
};

//getting all cards
export const getAllCards = async (listId) => {
  const get_cards_url = `${base_URL}/lists/${listId}/cards`;
  try {
    let res = await axios.get(get_cards_url);
    return res;
  } catch (error) {
    throw error;
  }
};

//adding a card
export const addCard = async (listId, newCardName) => {
  const add_card_url = `${base_URL}/cards?idList=${listId}`;
  try {
    let res = await axios.post(add_card_url, {
      name: newCardName,
    });
    return res;
  } catch (error) {
    throw error;
  }
};

//adding a checklist
export const addChecklist = async (cardId, newChecklistName) => {
  const add_checklist_url = `${base_URL}/checklists?idCard=${cardId}`;
  try {
    let res = await axios.post(add_checklist_url, {
      name: newChecklistName,
    });
    return res;
  } catch (error) {
    throw error;
  }
};

//deleting a card
export const deleteCard = async (cardId) => {
  const delete_card_url = `${base_URL}/cards/${cardId}`;
  try {
    await axios.delete(delete_card_url);
  } catch (error) {
    throw error;
  }
};

//getting all checklists
export const getChecklists = async (cardId) => {
  const get_checklists_url = `${base_URL}/cards/${cardId}/checklists`;
  try {
    let res = await axios.get(get_checklists_url);
    return res;
  } catch (error) {
    throw error;
  }
};

//deleting a checklist
export const deleteChecklist = async (checklistId) => {
  const delete_checklist_url = `${base_URL}/checklists/${checklistId}`;
  try {
    await axios.delete(delete_checklist_url);
  } catch (error) {
    throw error;
  }
};

//complete_check
export const completeCheck = async (cardId, checklistId, itemId) => {
  const complete_check_url = `${base_URL}/cards/${cardId}/checklist/${checklistId}/checkItem/${itemId}?state=complete`;
  try {
    let res1 = await axios.put(complete_check_url);
  } catch (error) {
    throw error;
  }
};

//incomplete_check
export const incompleteCheck = async (cardId, checklistId, itemId) => {
  const incomplete_check_url = `${base_URL}/cards/${cardId}/checklist/${checklistId}/checkItem/${itemId}?state=incomplete`;
  try {
    let res2 = await axios.put(incomplete_check_url);
  } catch (error) {
    throw error;
  }
};

//getting all items
export const getItems = async (checklistId) => {
  const get_items_url = `${base_URL}/checklists/${checklistId}/checkItems`;
  try {
    let res = await axios.get(get_items_url);
    return res;
  } catch (error) {
    throw error;
  }
};

//adding an item
export const addItem = async (checklistId, newItem) => {
  const add_item_url = `${base_URL}/checklists/${checklistId}/checkItems?name=${newItem}`;
  try {
    let res = await axios.post(add_item_url);
    return res;
  } catch (error) {
    throw error;
  }
};

//deleting an item
export const deleteItem = async (checklistId, itemId) => {
  const delete_item_url = `${base_URL}/checklists/${checklistId}/checkItems/${itemId}`;
  try {
    await axios.delete(delete_item_url);
  } catch (error) {
    throw error;
  }
};

//deleting a list
export const deleteList = async (listId) => {
  const delete_list_url = `${base_URL}/lists/${listId}/closed?value=true`;
  try {
    await axios.put(delete_list_url);
  } catch (error) {
    throw error;
  }
};

// adding a list
export const addList = async (newList, id) => {
  const add_list_url = `${base_URL}/lists?name=${newList}&idBoard=${id}&key=${apiKey}&token=${token}`;
  try {
    let res = await axios.post(add_list_url);
    return res;
  } catch (error) {
    throw error;
  }
};
