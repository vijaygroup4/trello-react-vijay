import React from "react";
import useStyles from "../styles/style";

const PageNotFound = () => {
  const classes = useStyles();

  return (
    <div>
      <h4 className={classes.notFound}>Page Not Found.Please check the URL...</h4>
    </div>
  );
};

export default PageNotFound;
