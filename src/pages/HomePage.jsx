import React, { useEffect, useState, useContext } from "react";
import Header from "../components/Header";
import Boards from "../components/boards/Boards";
import CircularProgress from "@mui/material/CircularProgress";
import Box from "@mui/material/Box";
import useStyles from "../styles/style";
import ErrorContext from "../contexts/ErrorContext";
import { getBoards as gettingBoards } from "../api";

const HomePage = () => {
  const classes = useStyles();
  const [boards, setBoards] = useState([]);
  const { error, setError, isLoading, setIsLoading } = useContext(ErrorContext);

  useEffect(() => {
    //function that fetches all boards
    async function getBoards() {
      try {
        setIsLoading(true);
        let boards = await gettingBoards();
        setBoards(boards.data);
        setIsLoading(false);
      } catch (error) {
        setIsLoading(false);
        setError({ message: "while fetching Boards" });
        console.log("error=", error);
      }
    }
    getBoards();
  }, []);
  return (
    <>
      {error ? (
        <p className={classes.errorPage}>Error occured:{error.message}</p>
      ) : (
        <div>
          <Header></Header>
          <div className={classes.toolbar}></div>
          <Boards boards={boards} setBoards={setBoards}></Boards>
          {isLoading && (
            <Box sx={{ display: "flex", justifyContent: "center" }}>
              <CircularProgress />
            </Box>
          )}
        </div>
      )}
    </>
  );
};

export default HomePage;
