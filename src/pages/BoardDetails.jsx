import React, { useEffect, useState, useContext, useReducer } from "react";
import { useParams, useLocation } from "react-router-dom";
import useStyles from "../styles/style";
import { Box, CircularProgress, IconButton, Typography, Button, TextField, Snackbar } from "@mui/material";
import SnackContext from "../contexts/SnackContext";
import Lists from "../components/Lists";
import CloseIcon from "@mui/icons-material/Close";
import AddIcon from "@mui/icons-material/Add";
import ErrorContext from "../contexts/ErrorContext";
import { getLists, addList } from "../api";

const BoardDetails = () => {
  const { error, setError, isLoading, setIsLoading } = useContext(ErrorContext);
  const { open, message, showSnackbar, closeSnackbar } = useContext(SnackContext);
  const { state } = useLocation();
  const boardName = state ? state.boardName : "";
  const [isInputVisible, setInputVisible] = useState(false);
  const [listsDetails, setListDetails] = useState([]);
  const [newList, setNewList] = useState("");
  const classes = useStyles();
  const { id } = useParams();

  useEffect(() => {
    //function that fetches a board details
    async function fetchLists() {
      try {
        setIsLoading(true);
        let lists = await getLists(id);
        lists.data.reverse();
        setListDetails(lists.data);
        setIsLoading(false);
      } catch (error) {
        setIsLoading(false);
        setError({ message: "while fetching lists" });
        console.log("error occured while fetching lists:", error);
      }
    }
    fetchLists();
  }, []);

  const toggleInputVisibility = () => {
    setInputVisible(!isInputVisible);
  };

  //function that creates a list
  const addListHandler = async () => {
    try {
      let newListAdded = await addList(newList, id);
      setListDetails([...listsDetails, newListAdded.data]);
      setNewList("");
      toggleInputVisibility();
      showSnackbar("successfully created the list");
    } catch (error) {
      setError({ message: "while creating new list" });
      console.log("error while adding new list", error);
    }
  };

  return (
    <>
      {error ? (
        <p className={classes.errorPage}>Error occured:{error.message}</p>
      ) : (
        <div>
          <div style={{ backgroundImage: `url(https://source.unsplash.com/random/${id})`, backgroundSize: "cover", backgroundRepeat: "no-repeat", minHeight: "100vh", overflowY: "hidden" }}>
            <div className={classes.toolbar}></div>
            {isLoading && (
              <Box sx={{ display: "flex", justifyContent: "center" }}>
                <CircularProgress />
              </Box>
            )}

            <Typography color="white" gutterBottom sx={{ textAlign: "center", paddingTop: 1, textShadow: "2px 2px 4px rgba(0, 0, 0, 0.8)", fontSize: "1.3rem" }}>
              {boardName} Details
            </Typography>
            {/* Displaying all lists */}
            <Box display="flex" gap={2} paddingLeft={1}>
              <Lists listsDetails={listsDetails} setListDetails={setListDetails} />
              <Box>
                {!isInputVisible && (
                  <Button
                    startIcon={<AddIcon></AddIcon>}
                    onClick={toggleInputVisibility}
                    sx={{
                      marginBottom: 1,
                      color: "white",
                      textShadow: "1px 1px 2px rgba(0, 0, 0, 0.8)",
                      backgroundColor: "rgba(100, 100, 100, 0.2)",
                      textTransform: "none",
                      minWidth: 250,
                      textAlign: "start",
                      justifyContent: "flex-start",
                    }}
                    variant="contained"
                  >
                    Add another list
                  </Button>
                )}
                {isInputVisible && (
                  <Box sx={{ display: "flex", flexDirection: "column" }}>
                    <form
                      onSubmit={(e) => {
                        e.preventDefault();
                        addListHandler();
                      }}
                    >
                      <TextField
                        label="Enter List title"
                        variant="outlined"
                        size="small"
                        InputLabelProps={{ sx: { color: "white" } }}
                        InputProps={{ sx: { color: "white" } }}
                        value={newList}
                        onChange={(e) => setNewList(e.target.value)}
                        sx={{ minWidth: 200 }}
                        autoFocus
                        autoComplete="off"
                        required
                      />
                      <Box sx={{ display: "flex", alignItems: "center", marginTop: 1 }}>
                        <Button variant="contained" color="primary" sx={{ textTransform: "none" }}>
                          Add
                        </Button>
                        <IconButton onClick={toggleInputVisibility}>
                          <CloseIcon color="error" />
                        </IconButton>
                      </Box>
                    </form>
                  </Box>
                )}
              </Box>

              {listsDetails.length === 0 && !isLoading && !error && (
                <Box sx={{ display: "flex", justifyContent: "center", alignItems: "center", paddingLeft: 50, paddingTop: 10 }}>
                  <Typography color="white" sx={{ textAlign: "center", textShadow: "2px 2px 4px rgba(0, 0, 0, 0.8)" }}>
                    Oops! No Lists Found. Please add...
                  </Typography>
                </Box>
              )}
            </Box>

            <Snackbar open={open} autoHideDuration={2000} onClose={closeSnackbar} message={message}></Snackbar>
          </div>
        </div>
      )}
    </>
  );
};

export default BoardDetails;
