import React, { useState } from "react";
import ErrorContext from "./ErrorContext";

const ErrorContextProvider = ({ children }) => {
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState("");

  return <ErrorContext.Provider value={{ error, setError, isLoading, setIsLoading }}>{children}</ErrorContext.Provider>;
};

export default ErrorContextProvider;
