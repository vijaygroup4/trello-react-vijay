import React, { useState } from "react";
import SnackContext from "./SnackContext";

const SnackContextProvider = ({ children }) => {
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState("");

  const showSnackbar = (msg) => {
    setMessage(msg);
    setOpen(true);
  };

  const closeSnackbar = () => {
    setOpen(false);
  };

  return <SnackContext.Provider value={{ open, message, showSnackbar, closeSnackbar }}>{children}</SnackContext.Provider>;
};

export default SnackContextProvider;
