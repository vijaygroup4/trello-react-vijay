import React, { createContext } from "react";

const SnackContext = createContext({});

export default SnackContext;
