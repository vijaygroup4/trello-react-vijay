import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  toolbar: {
    marginTop: "4rem",
  },
  imageDiv: {
    width: "90px",
    height: "60px",
  },
  image: {
    width: "120%",
    height: "100%",
    objectFit: "contain",
  },
  boardsLink: {
    textDecoration: "none",
    color: "white",
  },
  errorPage: {
    textAlign: "center",
    marginTop: "5rem",
    color: "white",
    textShadow: "inherit",
    color: "black",
  },
  fixedDialog: {
    minHeight: "600px",
  },
  notFound: {
    marginTop: "5rem",
    textAlign: "center",
  },
});

export default useStyles;
